# Vend SSIS Integration Project

This SSIS package downloads all data from a Vend instance (via version 0.x of the Vend API) to a 
SQL Server database for querying from other systems or reporting.

## Development Notes

### !!!IMPORTANT!!!

If you modify the DownloadVendData.dtsx package and change any of the following variables make sure you reset them back to their defaults BEFORE uploading to BitBucket (for security reasons):

| Variable Name		   | Default Value		   |
|:------------------------ | ----------------------------- |
| DestinationDatabase	   | <DATABASE>			   |
| DestinationServer	   | <DATABASE_SERVER>	   	   |
| VendInstanceUrl	   | https://<RETAILER>.vendhq.com |
| VendLoginPassword	   | <PASSWORD>			   |
| VendLoginUsername	   | <USERNAME>			   |
| VendRequestPageDelay	   | 2.0			   |
| VendRequestTimeout	   | 200			   |

These variables should be overriden in the SQL Agent Job (see Deployment Notes below), thus specific values should not be hard-coded in the package.

### Common Files

The ".cs" following files in the Miscellaneous folder are the master copies of those used in the Script Component sources in the Data Flow tasks: 

| Miscellaneous\   |									|
| ---------------- | ------------------------------------------------------------------	|
| VendApi0.cs	   | Vend API Version 0.x JSON Deserialization Classes			|
| VendApi1.cs      | Vend API Version 1.0 JSON Deserialization Classes			|
| VendApiHelper.cs | Helper class to provide a generic way of accessing the Vend API	|

If you need to update these files make sure you copy them to each of the Script Component sources. This was preferable than creating and referencing a custom DLL library, which has it's own deployment issues.

## Deployment Notes

Do the following to deploy the project to an SQL Server instance:

#### Uploading the package to SQL Server Integration Services
1. Open SQL Server Management Studio and connect to Integration Services on the server.
1. Under Stored Packages > MSDB > Integrations, right click and select Import Package... (this is also used to update an existing package)
1. Select FileSystem, and browse or enter the path to the package (e.g. ...\SSIS Projects\VendSSISIntegrations\VendSSISIntegrations\DownloadVendData.dtsx)
1. Click OK to upload the package. If prompted to overwrite an existing package click Yes.

#### Scheduling the package to run
1. In SQL Server Management Studio, connect to the Database Engine on the server
1. Open the SQL Server Agent section, right click on Jobs and select New Job...
   (if you don't see SQL Server Agent, make sure it is installed and running)
1. Enter a suitable Name the job (usually name it after the database it will be writing to)
1. Click on the Steps page (near the top left of the dialog), and click New...
1. In the New Job Step dialog, give the step a useful name (eg Run SSIS Task) and select SQL Server Integration Services Package from the Type dropdown.
1. Set Package Source to SSIS Package Store and enter the server name make sure Use Windows Authentication is selected.
1. Click the ... button to browse for the package or enter: \Integrations\DownloadVendData
1. Enter the following in the Set values tab:

   | Property Path 				    | Value 									|
   | -----------------------------------------------| --------------------------------------------------------------------------|
   | \package.Variables[DestinationServer].Value    | The name of the database server to download the data to                   |
   | \package.Variables[DestinationDatabase].Value  | The name of the database to download the data to				|
   | \package.Variables[VendInstanceURL].Value	    | The URL to your Vend instance						|
   | \package.Variables[VendLoginUsername].Value    | Your Vend username (This should be a low privilege read-only account)	|
   | \package.Variables[VendLoginPassword].Value    | The Vend user's password							|
   | \package.Variables[VendRequestTimeout].Value   | Defaults to 200 Seconds. Increase value if you are getting timeout errors	|
   | \package.Variables[VendRequestPageDelay].Value | Defaults to 2 Seconds. Increase value if you are getting "connection was forcibly closed" errors	|
   
1. Click OK to create the step   
1. Back on the New Job dialog, click the Schedules page and either: click New... to create a new schedule for this job, or click Pick to select an existing one.
1. If you want to send an email if the job fails, click on the Notifications page and enable the E-mail row. Select the group or user to be notified
1. Click OK to save the job.
