using System;
using System.Collections.Generic;
using System.Text;

namespace CrossroadsFoundation.Integrations.Vend.Api
{
    public class Pagination
    {
        public int results { get; set; }
        public int page { get; set; }
        public int page_size { get; set; }
        public int pages { get; set; }
    }

    public abstract class PaginatedResponse : IPaginatedResponse
    {
        public Pagination pagination { get; set; }
    }

    /* === Vend API Version 0.x ===
        Stock Control
            GET /api/stock_movements
            GET /api/consignment
            GET /api/consignment_product
        Customers
            GET /api/customers
        Payment Types
            GET /api/payment_types
        Products
            GET /api/products
        Register Sales
            GET /api/register_sales
            GET /api/register_sales/register_sale_id
        Registers
            GET /api/registers
        Suppliers
           GET /api/supplier     
        Taxes
            GET /api/taxes
        Users
            GET /api/users
    */

    #region GET /api/products

    public class ProductsResponse : PaginatedResponse
    {
        public Product[] products { get; set; }
    }

    public class Product
    {
        public Guid id { get; set; }
        public string source_id { get; set; }
        public string handle { get; set; }
        public string type { get; set; }
        public string variant_source_id { get; set; }
        public string variant_parent_id { get; set; }
        public string variant_option_one_name { get; set; }
        public string variant_option_one_value { get; set; }
        public string variant_option_two_name { get; set; }
        public string variant_option_two_value { get; set; }
        public string variant_option_three_name { get; set; }
        public string variant_option_three_value { get; set; }
        public bool active { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string image { get; set; }
        public string image_large { get; set; }
        public string sku { get; set; }
        public string tags { get; set; }
        public Guid? brand_id { get; set; }
        public string brand_name { get; set; }
        public string supplier_name { get; set; }
        public string supplier_code { get; set; }
        public decimal supply_price { get; set; }
        public string account_code_purchase { get; set; }
        public string account_code_sales { get; set; }
        public Inventory[] inventory { get; set; }
        public PriceBookEntry[] price_book_entries { get; set; }
        public decimal price { get; set; }
        public decimal tax { get; set; }
        public Guid tax_id { get; set; }
        public decimal tax_rate { get; set; }
        public string tax_name { get; set; }
        public int display_retail_price_tax_inclusive { get; set; }
        public DateTime updated_at { get; set; }
        public DateTime? deleted_at { get; set; }

        public Api1.Brand brand
        {
            get { return (brand_id.HasValue) ? new Api1.Brand() { id = brand_id.Value, name = brand_name } : null; }
        }
    }

    public class Inventory
    {
        public Guid product_id { get; set; }
        public Guid outlet_id { get; set; }
        public string outlet_name { get; set; }
        public decimal count { get; set; }
        public int? reorder_point { get; set; }
        public int? restock_level { get; set; }

        public Api1.Outlet outlet
        {
            get { return new Api1.Outlet() { id = outlet_id, name = outlet_name }; }
        }
    }

    public class PriceBookEntry
    {
        public Guid id { get; set; }
        public Guid product_id { get; set; }
        public Guid price_book_id { get; set; }
        public string price_book_name { get; set; }
        public string type { get; set; }
        public Guid? outlet_id { get; set; }
        public string outlet_name { get; set; }
        public Guid customer_group_id { get; set; }
        public string customer_group_name { get; set; }
        public decimal price { get; set; }
        public decimal? loyalty_value { get; set; }
        public decimal tax { get; set; }
        public Guid tax_id { get; set; }
        public string tax_name { get; set; }
        public decimal tax_rate { get; set; }
        public int display_retail_price_tax_inclusive { get; set; }
        public decimal? min_units { get; set; }
        public decimal? max_units { get; set; }
        public DateTime? valid_from { get; set; }
        public DateTime? valid_to { get; set; }

        public Api1.Outlet outlet
        {
            get { return (outlet_id.HasValue) ? new Api1.Outlet() { id = outlet_id.Value, name = outlet_name } : null; }
        }
    }

    #endregion

    #region GET /api/register_sales

    public class RegisterSalesResponse : PaginatedResponse
    {
        public RegisterSale[] register_sales { get; set; }
    }

    public class RegisterSale
    {
        public Guid id { get; set; }
        public Guid register_id { get; set; }
        public int market_id { get; set; }
        public Guid customer_id { get; set; }
        public string customer_name { get; set; }
        //public Customer customer { get; set; } // Ignoring as GET /api/customers will get this data.
        public Guid user_id { get; set; }
        public string user_name { get; set; }
        public DateTime sale_date { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public decimal total_price { get; set; }
        public decimal total_cost { get; set; }
        public decimal total_tax { get; set; }
        public string tax_name { get; set; }
        public string note { get; set; }
        public string status { get; set; } // OPEN, CLOSED, SAVED, LAYBY, LAYBY_CLOSED, ONACCOUNT, ONACCOUNT_CLOSED, VOIDED
        public string short_code { get; set; }
        public int invoice_number { get; set; }
        public RegisterSaleProduct[] register_sale_products { get; set; }
        public Totals totals { get; set; }
        public RegisterSalePayment[] register_sale_payments { get; set; }
    }

    public class RegisterSaleProduct
    {
        public Guid id { get; set; }
        public Guid product_id { get; set; }
        public Guid register_id { get; set; }
        public int sequence { get; set; }
        public string handle { get; set; }
        public string sku { get; set; }
        public string name { get; set; }
        public int quantity { get; set; }
        public decimal price { get; set; }
        public int price_set { get; set; }
        public decimal? discount { get; set; }
        public decimal loyaly_value { get; set; }
        public decimal tax { get; set; }
        public Guid tax_id { get; set; }
        //public string tax_name { get; set; }
        public decimal tax_rate { get; set; }
        public decimal tax_total { get; set; }
        public decimal price_total { get; set; }
        public int display_retail_price_tax_inclusive { get; set; }
        public string status { get; set; } //..., VALID, CONFIRMED, ...
        public ProductAttribute[] attributes { get; set; }
    }

    public class Totals
    {
        public decimal total_price { get; set; }
        public decimal total_tax { get; set; }
        public decimal total_payment { get; set; }
        public decimal total_to_pay { get; set; }
    }

    public class RegisterSalePayment
    {
        public Guid id { get; set; }
        public int payment_type_id { get; set; }
        public Guid retailer_payment_type_id { get; set; }
        public string name { get; set; }
        public string label { get; set; }
        public DateTime payment_date { get; set; }
        public decimal amount { get; set; }
    }

    public class ProductAttribute
    {
        public string name { get; set; }
        public string value { get; set; }
    }

    #endregion

    #region GET /api/registers

    public class RegistersResponse
    {
        public Register[] registers { get; set; }
    }

    public class Register
    {
        public Guid id { get; set; }
        public string name { get; set; }
        public Guid outlet_id { get; set; }
        public int print_receipt { get; set; }
        public string receipt_header { get; set; }
        public string receipt_footer { get; set; }
        public string receipt_style_sheet { get; set; }
        public string invoice_prefix { get; set; }
        public string invoice_suffix { get; set; }
        public int invoice_sequence { get; set; }
        public int register_open_count_sequence { get; set; }
        public DateTime register_open_time { get; set; }
        public DateTime? register_close_time { get; set; }
    }

    #endregion

    #region GET /api/payment_types

    public class PaymentTypesResponse
    {
        public PaymentType[] payment_types { get; set; }
    }

    public class PaymentType
    {
        public Guid id { get; set; }
        public string name { get; set; }
        public int payment_type_id { get; set; }
        public Config config { get; set; }
    }

    public class Config
    {
        // No fields as yet
    }

    #endregion

    #region GET /api/users

    public class UsersResponse
    {
        public User[] users { get; set; }
    }

    public class User
    {
        public Guid id { get; set; }
        public string username { get; set; }
        public string name { get; set; }
        public Guid? outlet_id { get; set; }
        public string outlet_name { get; set; }
        public string account_type { get; set; }

        public Api1.Outlet outlet
        {
            get { return (outlet_id.HasValue) ? new Api1.Outlet() { id = outlet_id.Value, name = outlet_name } : null; }
        }
    }

    #endregion

    #region GET /api/supplier?page=...

    public class SuppliersResponse : Pagination, IPaginatedResponse
    {
        public Pagination pagination { get { return (Pagination)this; } }
        public Supplier[] suppliers { get; set; }
    }

    public class Supplier
    {
        public Guid id { get; set; }
        public Guid retailer_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string source { get; set; }
        public Contact contact { get; set; }
    }

    #endregion

    public class Contact
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string company_name { get; set; }
        public string phone { get; set; }
        public string mobile { get; set; }
        public string fax { get; set; }
        public string email { get; set; }
        public string twitter { get; set; }
        public string website { get; set; }
        public string physical_address_1 { get; set; }
        public string physical_address_2 { get; set; }
        public string physical_suburb { get; set; }
        public string physical_city { get; set; }
        public string physical_postcode { get; set; }
        public string physical_state { get; set; }
        public string physical_country_id { get; set; }
        public string postal_address_1 { get; set; }
        public string postal_address_2 { get; set; }
        public string postal_suburb { get; set; }
        public string postal_city { get; set; }
        public string postal_postcode { get; set; }
        public string postal_state { get; set; }
        public string postal_country_id { get; set; }
    }

    #region GET /api/customers

    public class CustomersResponse : PaginatedResponse
    {
        public Customer[] customers { get; set; }
    }

    public class Customer : Contact
    {
        public Guid id { get; set; }
        public string name { get; set; }
        public string customer_code { get; set; }
        public Guid customer_group_id { get; set; }
        public string customer_group_name { get; set; }

        // Contact details inherited from Contact

        public DateTime updated_at { get; set; }
        public DateTime? deleted_at { get; set; }
        public decimal balance { get; set; }
        public decimal year_to_date { get; set; }
        public DateTime? date_of_birth { get; set; }
        public int points { get; set; }
        public string sex { get; set; }
        public string custom_field_1 { get; set; }
        public string custom_field_2 { get; set; }
        public string custom_field_3 { get; set; }
        public string custom_field_4 { get; set; }
        public Contact contact
        {
            get { return this; }
        }
    }

    #endregion

    #region GET /api/stock_movements

    public class StockMovementsResponse
    {
        public StockMovement[] stock_movements { get; set; }
    }

    public class StockMovement
    {
        public Guid id { get; set; }
        public string name { get; set; }
        public string type { get; set; } // OUTLET, STOCKTAKE, SUPPLIER
        public DateTime date { get; set; }
        public Guid outlet_id { get; set; }
        public Guid? supplier_id { get; set; }
        public Guid? source_outlet_id { get; set; }
        public string status { get; set; } // RECEIVED, OPEN, CANCELLED, STOCKTAKE_COMPLETE, STOCKTAKE, SENT
        public DateTime? received_at { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public StockMovementProduct[] products { get; set; }
    }

    public class StockMovementProduct
    {
        public Guid id { get; set; }
        public Guid? product_id { get; set; }
        public string name { get; set; }
        public decimal count { get; set; }
        public decimal received { get; set; }
        public decimal cost { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }

    #endregion

    #region GET /api/consignment

    public class ConsignmentsResponse
    {
        public Consignment[] consignments { get; set; }
    }

    public class Consignment
    {
        public Guid id { get; set; }
        public string name { get; set; }
        public DateTime consignment_date { get; set; }
        public DateTime? due_at { get; set; }
        public DateTime? received_at { get; set; }
        public Guid retailer_id { get; set; }
        public Guid outlet_id { get; set; }
        public Guid? supplier_id { get; set; }
        public Guid? source_outlet_id { get; set; }
        public string status { get; set; } // RECEIVED, OPEN, CANCELLED, STOCKTAKE_COMPLETE, STOCKTAKE, SENT
        public string type { get; set; } // OUTLET, STOCKTAKE, SUPPLIER
        public string accounts_transaction_id { get; set; }
    }

    #endregion

    #region GET /api/consignment_product?[consignment_id=...|product_id=...]

    public class ConsignmentProductsResponse
    {
        public ConsignmentProduct[] consignment_products { get; set; }
    }

    public class ConsignmentProduct
    {
        public Guid id { get; set; }
        public Guid consignment_id { get; set; }
        public Guid? product_id { get; set; }
        public decimal count { get; set; }
        public decimal received { get; set; }
        public decimal cost { get; set; }
        public string status { get; set; }
        public int sequence_number { get; set; }
    }

    #endregion

    #region GET /api/taxes

    public class TaxesResponse
    {
        public Tax[] taxes { get; set; }
    }


    public class Tax
    {
        public Guid id { get; set; }
        public string name { get; set; }
        public decimal rate { get; set; }
    }

    #endregion
}
