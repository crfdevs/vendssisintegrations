using System;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using System.Text;
using System.Threading;

namespace CrossroadsFoundation.Integrations.Vend.Api
{
    public interface IPaginatedResponse
    {
        Pagination pagination { get; }
    }

    public class VendApiHelper
    {
        public delegate void ResultsPageProcessor<T>(T data);
        public delegate void Logger(string url, string message);

        public string Username { get; set; }
        public string Password { get; set; }
        public int Timeout { get; set; } // HTTP request timeout in seconds
        public float PageDelay { get; set; } // Seconds to pause before requesting a page.

        public VendApiHelper()
        {
            this.Timeout = 100; //seconds
            this.PageDelay = 1F; // seconds
        }

        public Logger LogInformation { get; set; }
        public Logger LogError { get; set; }

        public void RetrieveAll<T>(string webApiMethodUrl, ResultsPageProcessor<T> processResultsPage) where T : IPaginatedResponse
        {
            RetrieveAll<T>(webApiMethodUrl, "/page/{0}", processResultsPage);
        }
        public void RetrieveAll<T>(string webApiMethodUrl, string paginationParameterFormat, ResultsPageProcessor<T> processResultsPage) where T : IPaginatedResponse
        {
            HttpWebResponse response = null;
            var pageCount = 1;
            try
            {
                if (PageDelay > 0F)
                {
                    Thread.Sleep((int)(PageDelay * 1000)); // being kind to the Vend server
                }

                for (var pageId = 1; pageId <= pageCount; pageId++)
                {
                    response = CallRestfulWebservice(webApiMethodUrl + string.Format(paginationParameterFormat, pageId), "application/json", Username, Password, Timeout);
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var data = ParseJson<T>(response);
                        if (data.pagination != null)
                        {
                            pageCount = data.pagination.pages;
                            if (pageId == 1)
                            {
                                LogInformation(webApiMethodUrl, string.Format("Total results to process: {0}.", data.pagination.results));
                            }
                            LogInformation(webApiMethodUrl, string.Format("Processing results - page {0} of {1}.", pageId, pageCount));
                        }
                        processResultsPage(data);
                    }
                    else
                    {
                        LogError(webApiMethodUrl, "Unable to get data from webservice: HTTP status code " + response.StatusCode.ToString() + " returned.");
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                LogError(webApiMethodUrl, "Error processing data from webservice: " + e.ToString());
            }
        }

        public void Retrieve<T>(string webApiMethodUrl, ResultsPageProcessor<T> processResults) where T : class
        {
            HttpWebResponse response = null;
            try
            {
                response = CallRestfulWebservice(webApiMethodUrl, "application/json", Username, Password, Timeout);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var data = ParseJson<T>(response);
                    processResults(data);
                }
                else
                {
                    LogError(webApiMethodUrl, "Unable to get data from webservice: HTTP status code " + response.StatusCode.ToString() + " returned.");
                }
            }
            catch (Exception e)
            {
                LogError(webApiMethodUrl, "Error processing data from webservice: " + e.ToString());
            }
        }

        #region Generic REST / JSON Handling Code

        public static T ParseJson<T>(HttpWebResponse jsonResponse)
        {
            string json;
            using (var reader = new StreamReader(jsonResponse.GetResponseStream()))
            {
                json = reader.ReadToEnd();
            }

            var serializer = new JavaScriptSerializer();
            return serializer.Deserialize<T>(json);
        }

        public static HttpWebResponse CallRestfulWebservice(string url, string contentType, string username, string password, int timeout)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Headers.Add(HttpRequestHeader.Authorization, GetBasicAuthenticationValue(username, password));
            request.ContentType = contentType;
            request.AllowAutoRedirect = true;
            request.Timeout = timeout * 1000; //milliseconds
            request.Proxy = null;
            return (HttpWebResponse)request.GetResponse();
        }

        private static string GetBasicAuthenticationValue(string username, string password)
        {
            return "Basic " + Convert.ToBase64String(Encoding.ASCII.GetBytes(username + ":" + password));
        }

        #endregion
    }
}