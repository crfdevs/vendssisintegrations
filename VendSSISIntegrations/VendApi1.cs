using System;
using System.Collections.Generic;
using System.Text;

namespace CrossroadsFoundation.Integrations.Vend.Api1
{
    /* === Vend API Version 1.0 ===
        Consignment Products
            GET /api/1.0/consignment_product/consignment_product_id
        Consignments
            GET /api/1.0/consignment/consignment_id
        Customers
            GET /api/1.0/customer/customer_id
        Inventory
            GET /api/1.0/inventory/inventory_id
        Product Types
            GET /api/1.0/product_type/product_type_id
        Products
            GET /api/1.0/product/product_id
        Register Sales
            GET /api/1.0/register_sale/register_sale_id
        Suppliers
            GET /api/1.0/supplier/supplier_id
        User
            GET /api/1.0/user/user_id
    */

    #region GET /api/1.0/product/product_id

    public class Product
    {
        public Guid id { get; set; }
        public Guid retailer_id { get; set; }
        public string sku { get; set; }
        public string handle { get; set; }
        public string source { get; set; }
        public string source_id { get; set; }
        public bool active { get; set; }
        public string name { get; set; }
        public string description { get; set; }

        public ProductType product_type { get; set; }
        public Supplier supplier { get; set; }
        public Brand brand { get; set; }

        public Inventory[] inventory { get; set; }
        public PriceBookEntry[] price_book_entries { get; set; }

        // Not in Spec but for completeness
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }

    public class ProductType
    {
        public Guid id { get; set; }
        public Guid retailer_id { get; set; }
        public string name { get; set; }
    }

    public class Brand
    {
        public Guid id { get; set; }
        public string name { get; set; }
    }

    public class Inventory
    {
        public Guid id { get; set; }
        public Guid product_id { get; set; }
        public Guid outlet_id { get; set; }
        public decimal attributed_cost { get; set; }
        public decimal count { get; set; }
        public int? reorder_point { get; set; }
        public int? restock_level { get; set; }

        public Product product { get; set; }
        public Outlet outlet { get; set; }
    }

    public class PriceBookEntry
    {
        public Guid id { get; set; }
        public Guid product_id { get; set; }
        public decimal? min_units { get; set; }
        public decimal? max_units { get; set; }
        public decimal price { get; set; }
        public decimal tax { get; set; }
        public string type { get; set; }
        public Guid customer_group_id { get; set; }
        public string customer_group_name { get; set; }
        public Guid tax_id { get; set; }
        public string tax_name { get; set; }
        public decimal tax_rate { get; set; }
    }

    public class Outlet
    {
        public Guid id { get; set; }
        public string name { get; set; }
        public string time_zone { get; set; }
    }

    #endregion

    #region GET /api/1.0/register_sale/register_sale_id

    public class RegisterSale
    {
        public Guid id { get; set; }
        public DateTime sale_date { get; set; }
        public string status { get; set; } // OPEN, CLOSED, SAVED, LAYBY, LAYBY_CLOSED, ONACCOUNT, ONACCOUNT_CLOSED, VOIDED
        public Guid user_id { get; set; }
        public Guid customer_id { get; set; }
        public Guid register_id { get; set; }
        public int market_id { get; set; } //??? string
        public int invoice_number { get; set; } //??? string
        public string short_code { get; set; }
        public Totals totals { get; set; }
        public string note { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public Customer customer { get; set; }
        public User user { get; set; }
        public RegisterSaleProduct[] register_sale_products { get; set; }
        public RegisterSalePayment[] register_sale_payments { get; set; }
    }

    public class Totals : Api.Totals
    {
    }

    public class RegisterSaleProduct
    {
        public Guid id { get; set; }
        public Guid product_id { get; set; }
        public int quantity { get; set; }
        public decimal price { get; set; }
        public bool price_set { get; set; }
        public decimal tax { get; set; }
        public decimal price_total { get; set; }
        public decimal tax_total { get; set; }
        public Guid tax_id { get; set; }
        public string tax_name { get; set; }
        public decimal tax_rate { get; set; }
        public ProductAttribute[] attributes { get; set; }
    }

    public class RegisterSalePayment
    {
        public Guid id { get; set; }
        public DateTime payment_date { get; set; }
        public decimal amount { get; set; }
        public Guid retailer_payment_type_id { get; set; }
        public int payment_type_id { get; set; }
    }

    public class ProductAttribute : Api.ProductAttribute
    {
    }

    #endregion

    #region GET /api/1.0/user/user_id

    public class User
    {
        public Guid id { get; set; }
        public Guid retailer_id { get; set; }
        public string name { get; set; }
        public string display_name { get; set; }
        public Guid outlet_id { get; set; }
        public decimal? target_daily { get; set; }
        public decimal? target_weekly { get; set; }
        public decimal? target_monthly { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }

    #endregion

    #region GET /api/1.0/supplier/supplier_id

    public class Supplier : Api.Supplier
    {        
    }

    #endregion

    public class Contact : Api.Contact
    {
    }

    #region GET /api/1.0/customer/customer_id

    public class Customer
    {
        public Guid id { get; set; }
        public Guid retailer_id { get; set; }
        public string customer_code { get; set; }
        public decimal balance { get; set; }
        public int points { get; set; }
        public string note { get; set; }
        public decimal year_to_date { get; set; }
        public string sex { get; set; }
        public DateTime date_of_birth { get; set; }
        public string custom_field_1 { get; set; }
        public string custom_field_2 { get; set; }
        public string custom_field_3 { get; set; }
        public string custom_field_4 { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public Contact contact { get; set; }
        public string contact_first_Name { get; set; }
        public string contact_last_Name { get; set; }
    }

    #endregion

    #region GET /api/1.0/consignment/consignment_id

    public class Consignment
    {
        public Guid id { get; set; }
        public string name { get; set; }
        public DateTime consignment_date { get; set; }
        public DateTime due_at { get; set; }
        public DateTime received_at { get; set; }
        public Guid retailer_id { get; set; }
        public Guid outlet_id { get; set; }
        public Guid supplier_id { get; set; }
        public Guid source_outlet_id { get; set; }
        public string status { get; set; } // RECEIVED, OPEN, CANCELLED, STOCKTAKE_COMPLETE, STOCKTAKE, SENT
        public string type { get; set; }
        public Guid accounts_transaction_id { get; set; }
    }

    #endregion

    #region GET /api/1.0/consignment_product/consignment_product_id

    public class ConsignmentProduct
    {
        public Guid id { get; set; }
        public Guid consignment_id { get; set; }
        public Guid product_id { get; set; }
        public int count { get; set; }
        public int received { get; set; }
        public decimal cost { get; set; }
        public string status { get; set; } // ..., PENDING, ... 
        public int sequence_number { get; set; }
    }

    #endregion
}
